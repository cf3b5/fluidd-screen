# Fluidd Screen
针对树莓派的LCD屏幕做的优化版本
![Fluidd Screen](.github/images/1627960250052.jpg?raw=true "Fluidd")
![Fluidd Screen](.github/images/1627960293622.jpg?raw=true "Fluidd")
![Fluidd Screen](.github/images/1627960331747.jpg?raw=true "Fluidd")

## 优化
* 取消所有拖动的输入方式，防止滑动页面时候误触
* 屏蔽所有图表的点击&触摸事件，优化屏幕上滑动效果，防止误触
* 增加触屏的数字小键盘，并且只留下需要输入数字的功能输入框
* 打印时默认首页是打印状态页面，专门显示模型缩略图，时间进度，温度等信息
* 增加工具页，专门用来操作打印机移动，温度，开关灯和风扇等功能
* 将打印机加速度，系统回抽等功能转移至和bedmesh一个页面，这个页面统一进行打印机调整
* 系统页面上增加强制刷新页面，重启、关闭树莓派系统和重启moonraker和重启klipper服务按键
* 顶部标题栏右边，显示打印头和热床温度，以及打印机当前状态
* 移除配置页面，移除摄像头模块，移除需要全键盘才能操作的功能，譬如编辑文件，改文件名等功能
* 移除Gcode控制台的输入功能，只能在屏幕上查看Gcode控制台指令
* 中文汉化语言包
* 提交给moonraker的配置数据库命名空间改成fluidd_screen，默认打包的路径也是子目录screen，
  目的是希望可以根目录下的fluidd共存，省去单独配置新的nginx网站动作
  
## 安装
* 首先确认原本web的fluidd是正常工作的
* 将打包完成的文件，全部复制到/home/pi/fluidd/screen文件夹中（PS：注意，不是吧源码直接复制过去，打包的意思编译打包，如果会自己打包的，用仓库的源代码打包，不会打包的，用发行版压缩包解压后复制过去。）
* 访问http://[树莓派ip]/screen，测试能否正常访问
* 安装树莓派的Kiosk信息亭模式，其实就是安装最基础的图形界面，启动后自动打开chromium浏览器访问指定的网址
```shell
sudo apt-get install -y --no-install-recommends xserver-xorg xinit x11-xserver-utils
sudo apt-get install -y chromium-browser matchbox-window-manager xautomation unclutter ttf-wqy-microhei
```
* 在`/home/pi/`路径下创建一个kiosk的文件，里面的内容如下
```shell
#!/bin/sh
xset +dpms     # 打开能源之星节能模式
xset dpms 300 300 300
xset s off     # 禁止屏幕保护
xset s noblank # 避免黑屏
matchbox-window-manager -use_titlebar no &
unclutter -idle 0 &    # 自动隐藏鼠标
# 最后的网址就是chromium浏览器启动后自动打开的网址
chromium-browser --display=:0 --kiosk --enable-features=OverlayScrollbar --noerrdialogs  --incognito --window-position=0,0 http://localhost/screen/
```
* 赋予这个kiosk文件的执行权限
```shell
chmod +x ~/kiosk
```
* 编辑登陆自动运行脚本的文件，`/home/pi/.bashrc`（该文件默认为隐藏文件，需要显示所有文件才能看到），
  在该文件末尾增加下面的代码，其目的是让终端登陆后自动启动kiosk信息亭模式
```shell
case $(tty) in
  (/dev/tty[1-9])
        xinit /home/pi/kiosk -- -nocursor vt$(fgconsole) &
        ;;
esac
```  

* 使用raspi-config配置树莓派的Console Autologin终端自动登陆（注意是终端自动登陆，并不是Desktop Autologin桌面自动登录）

* 重启，看看重启后树莓派有没有自动登陆，并且启动浏览器打开fluidd screen界面

* 针对fluidd screen的配置，需要使用pc浏览`http://树莓派ip/screen`这个地址来配置

# Fluidd
Fluidd is a free and open-source Klipper web interface for managing your 3d printer.

![Fluidd](/.github/images/preview_sliced.png?raw=true "Fluidd")

## Features
- Responsive UI, supports desktop, tablets and mobile
- Customizable layouts. Move any panel where YOU want
- Built-in color themes
- Manage multiple printers from one Fluidd install
- [See our docs for more!](https://docs.fluidd.xyz)

## Support & Documentation
See our [Docs](https://docs.fluidd.xyz).
Join our [Discord!](https://discord.gg/GZ3D5tqfcF).

## Where to download?
You can download the latest release [here](https://github.com/cadriel/fluidd/releases/latest).

Older releases can be found [here](https://github.com/cadriel/fluidd/releases).

## Docker
We have an official docker image. This is updated for each release.
https://hub.docker.com/r/cadriel/fluidd

## How to use?
Fluidd is distributed as a RaspbiOS image, with Fluidd pre-installed and setup alongside its dependencies.

The recommended approach is to flash a fresh image using FluiddPI.

Please see the [docs](https://docs.fluidd.xyz) for help with installation and configuration.

### Credit
A big thank you to;
- the [Voron Community](http://vorondesign.com/)
- Kevin O'Connor for [Klipper](https://github.com/KevinOConnor/klipper)
- Eric Callahan for [Moonraker](https://github.com/Arksine/moonraker)
- Ray for [MainsailOS](https://github.com/raymondh2/MainsailOS)
